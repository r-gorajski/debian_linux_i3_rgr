Hej.

Ten projekt ma na celu opisanie sposobu instalacji Debian GNU Linux, wraz z kafelkowym środowiskiem graficznym i3wm.

Opis staram się przedstawić krok-po-kroku man nadzieję, że w sposób przystępny, wraz z komentarzami.

Dokładne informacje na temat i3wm wraz z niesamowicie dobrą dokumentacją znajdziesz na stronie autora projektu:
https://i3wm.org/

OK - czym wyróżnia się pozytywnie "mój" Debian?

- Jest spersonalizowany według moich upodobań, to jest najważniejsze :smile:.
- Cześć oprogramowania, chcę mieć w najnowszej wersji, np. Libre Office, Remmina, Firefox (nie Firefox ESR ).
- Pokaże także jak w moim odczuciu, fajnie spersonalizować między innymi zsh - tak będziemy używali zsh a nie bash.
- Pokażę także moją konfigurację i3wm, z czasem zapewne czytając dokumentację, urządzisz to po swojemu, lub od razu to zrobisz :smile:.
- Coś jeszcze pewnie by się znalazło, ale pisząc to nie pamiętam :smile:.

"Mój" Debian ma także pewne wady. Jest niestety swego rodzaju Frankensteinem :smile:

- Instalacja i konfiguracja oprogramowania z Git wymaga śledzenia zmian i ręcznej aktualizacji.
- Instalacja ręczna części oprogramowania np. Firefox / Libre Office, także wymagga śledzenia zamian i ręcznej aktualizacji.
- Instalacja Debian'a w sposób opisany poniżej, jest trochę czasochłonna i wymaga pewnej wiedzy.
- Coś jeszcze pewnie by się znalazło, ale pisząc to nie pamiętam :smile:.

W związku z powyższym, rozpoczniemy od instalacji minimalnej Debian'a, tak jakbyśmy chcieli z niego zrobić jakiś serwer.
Następnie doinstalujemy niezbędne pakiety. Pobierzemy trochę oprogramowania z Gitlab'a i Github'a
Skompilujemy co trzeba oraz pobawimy się w edycję kilku plików, tak aby po uruchomieniu PC i zalogowaniu się w lightdm, ukazał nam się upragniony pulpit i3wm. Pobierzemy także kilka binarnych pakietów i po prostu ręcznie je zainstalujemy.

Jeszcze jedna uwaga, w poniższym projekcie nie znajdziesz "samograja". Po sklonowaniu mojego projektu, nie odnajdziesz w nim pliku np. install.sh po odpaleniu, którego poczekasz trochę pijąc kawę i po restarcie PC będziesz miał nowy i piekny system operacyjny.

Mój projekt ma na celu, zapoznanie z instalacją krok po kroku, dzięki temu nabędziesz troszkę wiedzy (przynajmniej ja nabyłem, pisząc ten poradnik), oraz o ile lubisz podłubać w plikach konfiguracyjnych - będziesz dobrze się bawił (mam nadzieję). Nie oznacza to, że zostawię Ciebie na lodzie. Tam gdzie to możliwe ułatwię instalację pakietów poprzez mega proste skrypty. Nie widzę potrzeby ręcznego wpisywania "apt-get install ..." dla każdego niezbędnego pakietu.

Aby zacząć, przejdź do sekcji [Wiki](https://gitlab.com/r-gorajski/debian_linux_i3_rgr/-/wikis/home), czytaj, instaluj i baw się dobrze!!!
